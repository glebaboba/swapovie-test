export interface IJwtData {
  userId: number;
  email: string;
}
