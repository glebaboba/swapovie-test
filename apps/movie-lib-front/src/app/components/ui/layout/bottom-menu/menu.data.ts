export const menuData: any[] = [{
    path: 'Home',
    iconName: 'pause',
    },{
    path: 'Watchlist',
    iconName: 'staro',
    },{
    path: 'Charts',
    iconName: 'barschart',
    },{
    path: 'Settings',
    iconName: 'setting',
    }

]

