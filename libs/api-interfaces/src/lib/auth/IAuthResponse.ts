import { IUser } from '../prisma/IUser';

export interface IAuthResponse {
  user: IUser;
  accessToken: string;
}
