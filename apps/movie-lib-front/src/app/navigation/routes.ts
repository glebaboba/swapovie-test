import { IRoute } from './navigation.types';
import Home from '../components/screens/home/Home';
import Settings from '../components/screens/settings/Settings';
import Profile from '../components/screens/profile/Profile';
import Charts from '../components/screens/charts/Charts';
import Watchlist from '../components/screens/watchlist/Watchlist';

export const routes: IRoute[] = [
  {
    name: 'Home',
    component: Home,
  },
  {
    name: 'Settings',
    component: Settings,
  },
  {
    name: 'Profile',
    component: Profile,
  },
  {
    name: 'Charts',
    component: Charts,
  },
  {
    name: 'Watchlist',
    component: Watchlist,
  },
];
