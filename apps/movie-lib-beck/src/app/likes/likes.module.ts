import { Module } from '@nestjs/common';
import { LikeService } from './like.service';
import { LikesController } from './likes.controller';
import { PrismaService } from '../prisma/prisma.service';

@Module({
  controllers: [LikesController],
  providers: [LikeService, PrismaService],
  exports: [LikeService]
})
export class LikesModule {}
