import React from 'react';
import { Button, SafeAreaView, Text } from 'react-native';
import { useInjection } from 'inversify-react';
import { observer } from 'mobx-react';
import { DependencyType } from '../ioc/DependencyType';
import { IAuth } from '../models/interfaces';

export const Test = observer(() => {
  const auth = useInjection<IAuth>(DependencyType.Auth);

  const login = () => {
    auth.login('test1@test.com', 'test');
  };

  return (
    <SafeAreaView>
      <Text>Test</Text>

      <Text>{auth.user?.userId}</Text>

      <Button onPress={() => login()} title={'dfdgdgg'} />
    </SafeAreaView>
  );
});

export default Test;
