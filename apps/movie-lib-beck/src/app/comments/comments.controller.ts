import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CommentsDto } from './comments.dto';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentService: CommentsService) {}

  @Post('add/:userId/:movieId')
  async add(
    @Param('userId') userId: string,
    @Param('movieId') movieId: string,
    @Body() commentDto: CommentsDto
  ) {
    return this.commentService.add(userId, movieId, commentDto.commentText);
  }

  @Post('remove/:commentId')
  async remove(@Param('commentId') commentId: string) {
    return this.commentService.remove(commentId);
  }

  @Get('all/:movieId')
  async getLikes(@Param('movieId') movieId: string) {
    return this.commentService.getComments(movieId);
  }
}
