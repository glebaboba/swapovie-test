import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class LikeService {
  constructor(private prisma: PrismaService) {}

  async add(userId: string, movieId: string) {
    const user = await this.prisma.user.findUnique({
      where: {
        userId: Number(userId),
      },
    });
    if (!user) throw new NotFoundException('User does not exist');

    await this.prisma.like.create({
      data: {
        userId: Number(userId),
        movieId: Number(movieId),
      },
    });

    return this.prisma.like.findMany({
      where: {
        movieId: Number(movieId),
      },
    });
  }

  async remove(userId: string, movieId: string) {
    const likes = await this.prisma.like.findMany({
      where: {
        AND: [
          {
            userId: Number(userId),
          },
          {
            movieId: Number(movieId),
          },
        ],
      },
    });
    if (!likes.length) throw new NotFoundException('Like does not exist');

    await this.prisma.like.delete({
      where: {
        likeId: Number(likes[0].likeId),
      },
    });

    return this.prisma.like.findMany({
      where: {
        movieId: Number(movieId),
      },
    });
  }

  async getLikes(movieId: string) {
    const likes = await this.prisma.like.findMany({
      where: {
        movieId: Number(movieId),
      },
    });

    return likes || [];
  }

  async getLikesByUser(userId: string) {
    const likes = await this.prisma.like.findMany({
      where: {
        userId: Number(userId),
      },
    });

    return likes || [];
  }
}
