import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import {
  Injectable,
  UnauthorizedException,
  Dependencies,
} from '@nestjs/common';
import { UserService } from '../../user/user.service';
import { BcryptService } from '../bcrypt/bcrypt.service';
import { User } from '@prisma/client';
import { IUser } from '@movie-lib/api-interfaces';

@Injectable()
@Dependencies(BcryptService, UserService)
export class LocalStrategy extends PassportStrategy(Strategy) {
  private bcryptService: BcryptService;
  private userService: UserService;
  constructor(bcryptService: BcryptService, userService: UserService) {
    super({ usernameField: 'email' });
    this.bcryptService = bcryptService;
    this.userService = userService;
  }

  async validate(email: string, password: string): Promise<IUser> {
    const user = await this.userService.getUser({
      email,
    });

    if (user) {
      const isValidPassword = await this.bcryptService.comparePassword(
        password,
        user.password
      );

      if (isValidPassword) {
        return user;
      } else {
        throw new UnauthorizedException('Invalid password');
      }
    } else {
      throw new UnauthorizedException('Such a user does not exist');
    }
  }
}
