import { FC, PropsWithChildren } from 'react';
import { Pressable, View, Text, PressableProps } from 'react-native';
import cn from 'clsx';

interface IButton extends PressableProps {}

const Button: FC<PropsWithChildren<IButton>> = ({
  children,
  className,
  ...rest
}) => {
  return (
    <Pressable
      className={cn(
        'self-center bg-primary py-3 px-8 rounded-3xl',
        className
      )}
      {...rest}
    >
      <Text className="font-semibold text-[#282828] text-lg">{children}</Text>
    </Pressable>
  );
};

export default Button;
