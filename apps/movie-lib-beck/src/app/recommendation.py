from flask import Flask, request, jsonify, session
from flask_session import Session
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)

# Загрузка данных
def load_data(movies_file):
    movies = pd.read_csv(movies_file)
    return movies

# Построение TF-IDF матрицы для фильмов
def build_tfidf_matrix(movies):
    tfidf = TfidfVectorizer(stop_words='english')
    movies['combined_features'] = movies.apply(lambda x: ' '.join(x[col] for col in movies.columns[1:]), axis=1)
    tfidf_matrix = tfidf.fit_transform(movies['combined_features'])
    return tfidf_matrix

# Рекомендации на основе сходства косинуса
def recommend_movies(movies, moviesIds, tfidf_matrix):
    cosine_sim = linear_kernel(tfidf_matrix, tfidf_matrix)
    recommendations = []
    for movie_id in moviesIds:
        idx = movies[movies['movie_id'] == movie_id].index.max()

        # Check if idx is nan or not
        if not pd.isnull(idx):
            sim_scores = list(enumerate(cosine_sim[int(idx)]))  # Convert idx to integer
            sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
            sim_scores = sim_scores[1:11]  # Первая рекомендация - это сам фильм
            movie_indices = [i[0] for i in sim_scores]
            recommended_movies = movies.iloc[movie_indices]
            recommendations.extend(recommended_movies.to_dict(orient='records'))
        else:
            print("Movie ID", movie_id, "not found in the dataset.")
    return recommendations


@app.route('/receive_movies', methods=['POST'])
def receive_movies():
    moviesIds = request.json.get('moviesIds')
    session['moviesIds'] = moviesIds
    return "MoviesIds received successfully"

@app.route('/get_recommendations', methods=['GET'])
def get_recommendations():
    moviesIds = session.get('moviesIds', [])
    # Загрузка данных и генерация рекомендаций
    movies = load_data('apps/movie-lib-beck/src/app/movies_base/tmdb_5000_credits.csv')
    tfidf_matrix = build_tfidf_matrix(movies)
    recommendations = recommend_movies(movies, moviesIds, tfidf_matrix)
    # Возвращаем только идентификаторы фильмов
    recommendations_ids = [movie['movie_id'] for movie in recommendations]
    return jsonify(recommendations_ids)



if __name__ == "__main__":
    app.run(debug=True)
