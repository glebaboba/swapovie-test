import React, { FC, useState, useEffect, useRef } from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';

import { AntDesign } from '@expo/vector-icons';
import { WebView } from 'react-native-webview';
import { observer } from 'mobx-react';
import { IMovie } from '../../../models/interfaces/IMovie';
import { Moviescreen } from '../../Moviescreen';
import { useInjection } from 'inversify-react';
import { IAuth } from '../../../models/interfaces';
import { DependencyType } from '../../../ioc/DependencyType';
import { IUser } from '@movie-lib/api-interfaces';
import { Movie } from '../../../models/Movie'; // Import WebView from 'react-native'

import predefinedMovieIDs from '../../../../../assets/data.json'

const API_KEY = '1dd7405f04cbff94f340b1eb380d677e'; // Replace with your TMDB API key
const TMDB_BASE_URL = 'https://api.themoviedb.org/3';
const TMDB_DISCOVER_URL = `${TMDB_BASE_URL}/discover/movie`;
const TMDB_LATEST_MOVIE_URL = `${TMDB_BASE_URL}/movie/latest`;

/*const predefinedMovieIDs = [
  872585, 346698, 753342, 1429, 575264, 609681, 868759,
];*/

const Home = observer(() => {
  const [movieDetails, setMovieDetails] = useState<IMovie[]>([]);
  const [additionalMovies, setAdditionalMovies] = useState<IMovie[]>([]);
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const flatListRef = useRef<FlatList | null>(null);
  const [hasMoreMovies, setHasMoreMovies] = useState(true);
  const auth = useInjection<IAuth>(DependencyType.Auth);

  const screenWidth = Dimensions.get('window').width;
  const screenHeight = Dimensions.get('window').height;

  const fetchMovieVideo = async (movieId: number) => {
    try {
      const response = await fetch(
        `${TMDB_BASE_URL}/movie/${movieId}/videos?api_key=${API_KEY}`
      );
      const data = await response.json();

      if (data && data.results && data.results.length > 0) {
        return data.results[0].key;
      }

      // Return a default video key when no video data is found
      return 'dQw4w9WgXcQ';
    } catch (error) {
      console.error(`Error fetching video for movie ${movieId}.`, error);
      return 'dQw4w9WgXcQ'; // Use the default video key for error cases
    }
  };

  const fetchRandomMovies = async () => {
    try {
      const uniqueMovieIDs = new Set([
        ...movieDetails.map((movie) => movie.id),
        ...additionalMovies.map((movie) => movie.id),
      ]);

      while (true) {
        const randomMovieID =
          predefinedMovieIDs[
            Math.floor(Math.random() * predefinedMovieIDs.length)
          ];

        if (!uniqueMovieIDs.has(randomMovieID)) {
          try {
            const movieDataResponse = await fetch(
              `${TMDB_BASE_URL}/movie/${randomMovieID}?api_key=${API_KEY}`
            );
            const videoKey = await fetchMovieVideo(randomMovieID);

            if (movieDataResponse.ok && videoKey) {
              const movieData = await movieDataResponse.json();

              if (movieData.id) {
                // Fetch cast information
                const creditsResponse = await fetch(
                  `${TMDB_BASE_URL}/movie/${randomMovieID}/credits?api_key=${API_KEY}`
                );
                const creditsData = await creditsResponse.json();

                const newMovie = {
                  id: movieData.id,
                  title: movieData.title,
                  overview: movieData.overview,
                  videoKey,
                  posterPath: movieData.poster_path,
                  releaseYear: movieData.release_date
                    ? movieData.release_date.split('-')[0]
                    : '',
                  imdbRating: movieData.vote_average.toString(),
                  ageRating: 'PG-13',
                  genre: movieData.genres[0].name,
                  country: movieData.production_countries[0].name,
                  cast: creditsData.cast.map((castMember: any) => ({
                    id: castMember.id,
                    name: castMember.original_name,
                    profile_path: castMember.profile_path,
                  })),
                } as IMovie;

                setAdditionalMovies((prevAdditionalMovies) => [
                  ...prevAdditionalMovies,
                  newMovie,
                ]);
                uniqueMovieIDs.add(randomMovieID);
                break;
              }
            }
          } catch (error) {
            console.error(`Error fetching movie ${randomMovieID}.`, error);
          }
        }
      }
    } catch (error) {
      console.error(
        'Error fetching additional random movies from TMDB:',
        error
      );
    }
  };

  useEffect(() => {
    fetchRandomMovies();
  }, []);

  const onEndReachedThreshold = 0.5;

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ref={(ref) => (flatListRef.current = ref)}
        data={movieDetails.concat(additionalMovies)}
        keyExtractor={(item) => `movie_${item.id}`}
        renderItem={({ item: movie }) => {
          const movieModel = new Movie(
            movie.id.toString(),
            auth.user?.userId.toString() || ''
          );

          return (
            <Moviescreen
              movie={movie}
              activeUser={auth.user as IUser}
              movieModel={movieModel}
            />
          );
        }}
        horizontal={false}
        snapToInterval={screenHeight}
        decelerationRate={0.9}
        showsVerticalScrollIndicator={false}
        onEndReached={() => {
          if (hasMoreMovies) {
            fetchRandomMovies();
          }
        }}
        onEndReachedThreshold={onEndReachedThreshold}
        onMomentumScrollEnd={(event) => {
          if (event.nativeEvent.velocity) {
            const velocityY = event.nativeEvent.velocity.y;
            const newIndex = currentIndex + (velocityY > 0 ? 1 : -1);

            if (
              newIndex >= 0 &&
              newIndex < movieDetails.length + additionalMovies.length
            ) {
              setCurrentIndex(newIndex);

              const offset = newIndex * screenHeight;
              flatListRef.current?.scrollToIndex({
                index: newIndex,
                animated: true,
              });
            }
          }
        }}
      />
    </View>
  );
});

export default Home;
