import React, { useEffect, useState } from 'react';
import { useInjection } from 'inversify-react';
import Layout from '../../ui/layout/Layout';
import { IAuth } from '../../../models/interfaces';
import Button from '../../ui/Button';
import { DependencyType } from '../../../ioc/DependencyType';
import { IUser } from '../../../models/interfaces/IUser';
import { View, Text, ScrollView, Image, TouchableOpacity, Modal } from 'react-native';
import { IMovie } from '../../../models/interfaces/IMovie';
import { SafeAreaView } from 'react-native-safe-area-context';
import { IMovieModel } from '../../../models/interfaces/IMovieModel';
import { Comments } from '../../ui/layout/Comments';
import { AntDesign } from '@expo/vector-icons';
import { Movie } from '../../../models/Movie';

const API_KEY = '1dd7405f04cbff94f340b1eb380d677e'; // Replace with your TMDB API key
const TMDB_BASE_URL = 'https://api.themoviedb.org/3';

const Settings = () => {

  const [recommendations, setRecommendations] = useState<IMovie[]>([]);
  const [movieIds, setMovieIds] = useState<string[]>([]);

  const auth = useInjection<IAuth>(DependencyType.Auth);
  const user = useInjection<IUser>(DependencyType.User);

  const [showComments, setShowComments] = useState(false); // добавлено
  const [isClouded, setClouded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const moviesIds = await fetchMovieIds();
        setMovieIds(moviesIds);
      } catch (error) {
        console.error('Error fetching movie data:', error);
      }
    };

    fetchData();
  }, []);

  const fetchMovieIds = async () => {
    return await user.getMoviesByUserLikes(auth.user?.userId.toString() as string);
  };
  const fetchMovie = async (movieId: number) => {
    try {
      const response = await fetch(`${TMDB_BASE_URL}/movie/${movieId}?api_key=${API_KEY}`);
      if (!response.ok) {
        throw new Error(`Failed to fetch movie with ID ${movieId}`);
      }
      const movieData = await response.json();
      console.log("Movie data fetched successfully:", movieData); // Добавляем эту строку
      return {
        id: movieData.id,
        title: movieData.title,
        overview: movieData.overview,
        posterPath: movieData.poster_path,
        releaseYear: movieData.release_date ? movieData.release_date.split('-')[0] : '',
        imdbRating: movieData.vote_average.toString(),
        ageRating: 'PG-13',
        genre: movieData.genres[0].name,
        country: movieData.production_countries[0].name,
      } as IMovie;
    } catch (error) {
      console.error(`Error fetching movie with ID ${movieId}:`, error);
      return null;
    }
  };


  const handleUpdateRecommendations = async () => {
    try {
      console.log("Sending request to receive_movies");
      const response = await fetch('http://127.0.0.1:5000/receive_movies', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ moviesIds: movieIds }),
      });
      if (!response.ok) {
        throw new Error('Failed to update recommendations');
      }
      console.log("Request to receive_movies successful");

      console.log("Sending request to get_recommendations");
      const recommendationsResponse = await fetch('http://127.0.0.1:5000/get_recommendations');
      if (!recommendationsResponse.ok) {
        throw new Error('Failed to fetch recommendations from server');
      }
      console.log("Request to get_recommendations successful");

      const recommendationsData = await recommendationsResponse.json();

      // Получаем информацию о каждом фильме и устанавливаем в состояние recommendations
      const moviesInfo = await Promise.all(recommendationsData.map(movieId => fetchMovie(movieId)));
      setRecommendations(moviesInfo.filter(movie => movie !== null));

    } catch (error) {
      console.error('Error updating recommendations:', error);
    }
  };




  return (
    <SafeAreaView>
      <View style={{ flexDirection:'column', alignItems: 'center', alignContent: 'center' }}>
        <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', marginBottom:15}}>{auth.user?.email}</Text>
        <Button onPress={() => auth.logout()}>LOX</Button>
      </View>

      <View style = {{marginTop: 30}}>
        <Button onPress={handleUpdateRecommendations}>Update Recommendations</Button>
        <Text style={{ marginTop: 20, marginLeft: 15, color: 'white', fontSize: 30, fontWeight: 'bold', marginBottom:25 }}>Recommended Movies</Text>
        <ScrollView  contentContainerStyle={{ flexDirection: 'column', alignItems: 'center' }}>

          {recommendations.slice().map((movie,index) => {
            console.log(recommendations)
            return (
              <View key={index} style={{ padding: 5, backgroundColor: "#1c1c1c", marginBottom: 10 , borderRadius: 15, width: '90%', }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                  <View style={{ flex: 1, transform: [{ scale: 0.9 }], padding: 10 }}>
                    <View style={{ aspectRatio: 2 / 3, overflow: 'hidden' }}>
                      <Image
                        source={{ uri: `https://image.tmdb.org/t/p/w500/${movie.posterPath}` }}
                        style={{ width: '100%', height: '100%',  borderRadius: 20}}
                      />
                    </View>
                  </View>
                  <View style={{ flex: 3, paddingLeft: 10 }}>
                    {/* Text and information */}
                    <Text style={{ color: 'white' }}>
                      {movie.releaseYear + `  ${movie.imdbRating} (iMDB)`}
                    </Text>
                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>
                      {movie.title}
                    </Text>
                    <Text style={{ color: 'white' }}>{movie.genre}</Text>
                    <Text style={{ color: 'white' }}>{movie.ageRating}</Text>
                    <Text style={{ color: 'white' }}>{movie.country}</Text>
                  </View>
                </View>
                 {/*Block 2: Buttons */}

                <View
                  style={{
                    height: 'auto',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: '2%',
                    paddingTop: '5%',
                  }}
                >
                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      paddingRight: '3%',
                      paddingLeft: '2%',
                      flexDirection: 'row',
                      backgroundColor: '#1E1E1E',
                      minHeight: 35,
                      borderRadius: 7,
                      marginRight: '3%',
                    }}
                  >
                    <AntDesign
                      name="like1"
                      size={20}
                      color={'white'}
                      style={{ paddingRight: 5 }}
                    />
                    <Text style={{ color: 'white' }}>
                      LOX
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => setShowComments(true)} // изменено
                    style={{
                      alignItems: 'center',
                      paddingRight: '3%',
                      paddingLeft: '2%',
                      flexDirection: 'row',
                      backgroundColor: isClouded ? 'white' : '#1E1E1E',
                      minHeight: 35,
                      borderRadius: 7,
                      marginRight: '3%',
                    }}
                  >
                    <AntDesign
                      name="message1"
                      size={20}
                      color={isClouded ? '#1E1E1E' : 'white'}
                      style={{ paddingRight: 5 }}
                    />
                    <Text style={{ color: isClouded ? '#1E1E1E' : 'white' }}>
                      LOX
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

            )}
          )}
        </ScrollView>
      </View>


    </SafeAreaView>
  );
};

export default Settings;
