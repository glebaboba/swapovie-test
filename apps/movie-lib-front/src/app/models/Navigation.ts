import { makeAutoObservable } from 'mobx';
import { Navigation as ScreenNavigation } from 'react-native-navigation';
import { INavigation } from './interfaces';
import { injectable } from 'inversify';

@injectable()
export class Navigation implements INavigation {
  private componentId: string | null = null;

  constructor() {
    makeAutoObservable(this);
  }

  public setComponentId = (id: string): void => {
    this.componentId = id;
  };

  public pushToScreen = (screenName: string): void => {
    if (this.componentId) {
      ScreenNavigation.push(this.componentId, {
        component: {
          name: screenName,
        },
      });
    }
  };
}
