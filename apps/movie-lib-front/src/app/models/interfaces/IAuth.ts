import { IUser } from '@movie-lib/api-interfaces';

export interface IAuth {
  isLoading: boolean;
  user: IUser | null;
  login: (email: string, password: string) => Promise<void>;
  register: (email: string, password: string) => Promise<void>;
  logout: () => Promise<void>;
}
