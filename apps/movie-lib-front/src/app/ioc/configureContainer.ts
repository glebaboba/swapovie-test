import { Container } from 'inversify';
import { IAuth, INavigation } from '../models/interfaces';
import { DependencyType } from './DependencyType';
import { Auth } from '../models/Auth';
import { Navigation } from '../models/Navigation';
import {User} from "../models/User";
import {IUser} from "../models/interfaces/IUser";

export const configureContainer = (): Container => {
  const container = new Container({ defaultScope: 'Singleton' });

  container.bind<INavigation>(DependencyType.Navigation).to(Navigation);
  container.bind<IAuth>(DependencyType.Auth).to(Auth);
  container.bind<IUser>(DependencyType.User).to(User);

  return container;
};
