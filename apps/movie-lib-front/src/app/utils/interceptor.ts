import axios from 'axios';
import { getItemAsync } from 'expo-secure-store';

export const getContentType = () => ({
  'Content-Type': 'application/json',
});

const instance = axios.create({
  baseURL: process.env.EXPO_PUBLIC_SERVER_URL,
  headers: getContentType(),
});

instance.interceptors.request.use(async (config) => {
  const accessToken = await getItemAsync('accessToken');

  if (config.headers && accessToken)
    config.headers.Authorization = `Bearer ${accessToken}`;

  return config;
});

export default instance;
