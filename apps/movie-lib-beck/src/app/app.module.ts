import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { PrismaModule } from './prisma/prisma.module';
import { LikesModule } from './likes/likes.module';
import { CommentsModule } from './comments/comments.module';
import {MovieModule} from "./movie/movie.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PrismaModule,
    AuthModule,
    UserModule,
    LikesModule,
    CommentsModule,
    MovieModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
