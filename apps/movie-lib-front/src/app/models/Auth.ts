import { makeAutoObservable, runInAction } from 'mobx';
import { IAuth, INavigation } from './interfaces';
import { IAuthResponse, IUser } from '@movie-lib/api-interfaces';
import instance from '../utils/interceptor';
import { deleteItemAsync, setItemAsync } from 'expo-secure-store';
import { inject, injectable } from 'inversify';
import { DependencyType } from '../ioc/DependencyType';

@injectable()
export class Auth implements IAuth {
  private loading = false;

  private userData: IUser | null = null;

  public constructor(
    @inject(DependencyType.Navigation)
    private navigation: INavigation
  ) {
    makeAutoObservable(this);
  }

  public get isLoading(): boolean {
    return this.loading;
  }

  public get user(): IUser | null {
    return this.userData;
  }

  public async login(email: string, password: string): Promise<void> {
    this.loading = true;

    try {
      const response = await instance.post<IAuthResponse>('/auth/login', {
        email,
        password,
      });

      if (response.data.accessToken) {
        await setItemAsync('accessToken', response.data.accessToken);
      }

      runInAction(() => {
        this.userData = response.data.user;
        this.loading = false;
      });
    } catch (e) {
      console.log(e);
    }
  }

  public async register(email: string, password: string): Promise<void> {
    this.loading = true;

    try {
      const response = await instance.post<IAuthResponse>('/auth/register', {
        email,
        password,
        username: email,
      });

      if (response.data.accessToken) {
        await setItemAsync('accessToken', response.data.accessToken);
      }

      runInAction(() => {
        this.userData = response.data.user;
        this.loading = false;
      });
    } catch (e) {
      console.log(e);
    }
  }

  public async logout() {
    this.loading = true;
    this.userData = null;
    await deleteItemAsync('accessToken');
    runInAction(() => {
      this.loading = false;
    });
  }
}
