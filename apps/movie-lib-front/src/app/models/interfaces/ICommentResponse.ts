export interface ICommentResponse {
    commentId: number
    userId: number
    movieId: number
    text: string
}
