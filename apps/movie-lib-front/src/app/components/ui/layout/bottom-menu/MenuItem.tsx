import { FC } from 'react';
import { Pressable, Text, View } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

interface IMenuItemProps {
  item: any;
  nav: any;
  currentRoute?: string;
}

const MenuItem: FC<IMenuItemProps> = ({ currentRoute, nav, item }) => {
  const isActive = currentRoute === item.path;

  return (
    <Pressable onPress={() => nav(item.path)} className="w-[24%] items-center">
      <View style={{ alignItems: 'center' }}>
        <AntDesign
          name={item.iconName}
          size={26}
          color={isActive ? '#FFFFFF' : '#787878'}
        />
      </View>
    </Pressable>
  );
};

export default MenuItem;
