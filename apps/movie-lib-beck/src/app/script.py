import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

# Загрузка даних
def load_data(movies_file, liked_movies_file):
    movies = pd.read_csv(movies_file)
    liked_movies = pd.read_csv(liked_movies_file)
    return movies, liked_movies

# Побудова TF-IDF матриці для фільмів
def build_tfidf_matrix(movies):
    tfidf = TfidfVectorizer(stop_words='english')
    movies['combined_features'] = movies.apply(lambda x: ' '.join(x[col] for col in movies.columns[1:]), axis=1)
    tfidf_matrix = tfidf.fit_transform(movies['combined_features'])
    return tfidf_matrix

# Рекомендації на основі схожості косинуса
def recommend_movies(movies, liked_movies, tfidf_matrix):
    cosine_sim = linear_kernel(tfidf_matrix, tfidf_matrix)
    recommendations = []
    for movie_id in liked_movies['liked_movie_id']:
        idx = movies.index[movies['movie_id'] == movie_id][0]
        sim_scores = list(enumerate(cosine_sim[idx]))
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
        sim_scores = sim_scores[1:11]  # Перша рекомендація - це сам фільм
        movie_indices = [i[0] for i in sim_scores]
        recommendations.extend(movies['movie_id'].iloc[movie_indices])
    return recommendations

# Головна функція
def main():
    movies_file = './sample_data/tmdb_5000_credits.csv'
    liked_movies_file = './sample_data/likes.csv'

    movies, liked_movies = load_data(movies_file, liked_movies_file)
    tfidf_matrix = build_tfidf_matrix(movies)
    recommendations = recommend_movies(movies, liked_movies, tfidf_matrix)
    recommendations = pd.Series(recommendations).value_counts().reset_index()
    recommendations.columns = ['movie_id', 'recommendation_score']
    recommendations.to_csv('recommendations.csv', index=False)

if __name__ == "__main__":
    main()
