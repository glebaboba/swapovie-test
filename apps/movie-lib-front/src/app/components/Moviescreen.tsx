import { observer } from 'mobx-react';
import { IUser } from '@movie-lib/api-interfaces';
import { useState } from 'react';
import {
  Dimensions,
  FlatList,
  TouchableOpacity,
  View,
  Image,
  Text,
  Modal, // добавлено
} from 'react-native';
import { WebView } from 'react-native-webview';
import { AntDesign } from '@expo/vector-icons';
import { IMovieModel } from '../models/interfaces/IMovieModel';
import { Comments } from './ui/layout/Comments'; // добавлено
import { IMovie } from '../models/interfaces/IMovie';

interface IProps {
  movie: IMovie;
  activeUser?: IUser;
  movieModel: IMovieModel;
}

export const Moviescreen = observer((props: IProps) => {
  const [isClouded, setClouded] = useState(false);
  const [showComments, setShowComments] = useState(false); // добавлено

  const screenWidth = Dimensions.get('window').width;
  const screenHeight = Dimensions.get('window').height;

  const { movie, activeUser, movieModel } = props;

  const toggleLike = () => {
    movieModel.isLiked ? movieModel.removeLike() : movieModel.addLike();
  };

  return (
    <View style={{ flex: 1 }}>
      {movie.videoKey ? (
        <WebView
          source={{
            uri: `https://www.youtube.com/embed/${movie.videoKey}?controls=0&showinfo=0`,
          }}
          style={{ height: 200, width: screenWidth }}
        />
      ) : (
        <WebView
          source={{ uri: `https://www.youtube.com/embed/3JmEF1AdyXo` }}
          style={{ height: 200, width: screenWidth }}
        />
      )}
      <View style={{ width: screenWidth, height: screenHeight - 200 }}>
        {/* Block 1: Movie Details */}
        <View
          style={{
            height: screenHeight * 0.09,
            paddingTop: screenHeight * 0.03,
          }}
        >
          {/* Block 1: Movie Information */}
          <View
            style={{ flexDirection: 'row', alignItems: 'center', padding: 5 }}
          >
            <View style={{ flex: 1, transform: [{ scale: 0.9 }] }}>
              <View style={{ aspectRatio: 2 / 3, overflow: 'hidden' }}>
                <Image
                  source={{
                    uri: `https://image.tmdb.org/t/p/w500/${movie.posterPath}`,
                  }}
                  style={{ width: '100%', height: '100%' }}
                />
              </View>
            </View>

            <View style={{ flex: 3, paddingLeft: 10 }}>
              {/* Text and information */}
              <Text style={{ color: 'white' }}>
                {movie.releaseYear + `  ${movie.imdbRating} (iMDB)`}
              </Text>
              <Text
                style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}
              >
                {movie.title}
              </Text>
              <Text style={{ color: 'white' }}>{movie.genre}</Text>
              <Text style={{ color: 'white' }}>{movie.ageRating}</Text>
              <Text style={{ color: 'white' }}>{movie.country}</Text>
            </View>
          </View>

          {/*Block 2: Buttons */}

          <View
            style={{
              height: 'auto',
              flexDirection: 'row',
              alignItems: 'center',
              paddingLeft: '2%',
              paddingTop: '5%',
            }}
          >
            <TouchableOpacity
              onPress={toggleLike}
              style={{
                alignItems: 'center',
                paddingRight: '3%',
                paddingLeft: '2%',
                flexDirection: 'row',
                backgroundColor: movieModel.isLiked ? 'white' : '#1E1E1E',
                minHeight: 35,
                borderRadius: 7,
                marginRight: '3%',
              }}
            >
              <AntDesign
                name="like1"
                size={20}
                color={movieModel.isLiked ? '#1E1E1E' : 'white'}
                style={{ paddingRight: 5 }}
              />
              <Text style={{ color: movieModel.isLiked ? '#1E1E1E' : 'white' }}>
                {movieModel.likes.length}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => setShowComments(true)} // изменено
              style={{
                alignItems: 'center',
                paddingRight: '3%',
                paddingLeft: '2%',
                flexDirection: 'row',
                backgroundColor: isClouded ? 'white' : '#1E1E1E',
                minHeight: 35,
                borderRadius: 7,
                marginRight: '3%',
              }}
            >
              <AntDesign
                name="message1"
                size={20}
                color={isClouded ? '#1E1E1E' : 'white'}
                style={{ paddingRight: 5 }}
              />
              <Text style={{ color: isClouded ? '#1E1E1E' : 'white' }}>
                {movieModel.comments.length}
              </Text>
            </TouchableOpacity>
          </View>

           {/* Block 3: Cast */}
           <View style={{ padding: 10, paddingTop: 20, height: 200 }}>
            <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>
              Cast
            </Text>
            <FlatList
              data={movie.cast?.slice(0, 4) || []} // Displaying only the first 4 cast members
              keyExtractor={(actor) => `actor_${actor.id}`}
              renderItem={({ item: actor }) => (
                <View
                  style={{
                    alignItems: 'left',
                    paddingTop: 5,
                    width: 65,
                    marginRight: 25,
                  }}
                >
                  {actor.profile_path ? (
                    <Image
                      source={{
                        uri: `https://image.tmdb.org/t/p/w185/${actor.profile_path}`,
                      }}
                      style={{ width: 65, height: 90, borderRadius: 10 }}
                    />
                  ) : (
                    <Image
                      source={{
                        uri: `https://www.themoviedb.org/assets/2/v4/glyphicons/basic/glyphicons-basic-4-user-grey-d8fe957375e70239d6abdd549fd7568c89281b2179b5f4470e2e12895792dfa5.svg`,
                      }}
                      style={{ width: 65, height: 90, borderRadius: 10 }}
                    />
                  )}
                  <Text
                    style={{
                      color: 'white',
                      textAlign: 'left',
                      marginTop: 5,
                    }}
                    numberOfLines={2}
                  >
                    {actor.name}
                  </Text>
                </View>
              )}
              horizontal
              showsHorizontalScrollIndicator={false}
            />
          </View>

          {/* Block 4: Movie Description */}
          <View style={{ alignItems: 'left', paddingLeft: 10 }}>
            <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>
              Description
            </Text>
            <Text style={{ color: 'white' }} numberOfLines={3}>
              {movie.overview}
            </Text>
          </View>
        </View>
        </View>
     



      {/* Модальное окно для комментариев */}
      <Modal
        visible={showComments}
        transparent={true}
        animationType="slide"
        onRequestClose={() => setShowComments(false)}
      >
        <Comments
          comments={movieModel.comments}
          onClose={() => setShowComments(false)}
          addComment={(comment) => {
            movieModel.addComment(comment);
          }}
        />
      </Modal>

    </View>
  );
});
