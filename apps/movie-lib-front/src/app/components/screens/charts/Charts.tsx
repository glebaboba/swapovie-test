import React, { FC, useState, useEffect } from "react";
import { ScrollView, Text, View, Image, TouchableOpacity, Modal} from 'react-native';
import { IMovie } from "../../../models/interfaces/IMovie";
import { IAuth } from "../../../models/interfaces/IAuth";
import { IUser } from "../../../models/interfaces/IUser";
import { useInjection } from 'inversify-react';
import { DependencyType } from "../../../ioc/DependencyType";
import { SafeAreaView } from "react-native-safe-area-context";
import { AntDesign } from '@expo/vector-icons';
import { Movie } from "../../../models/Movie";
import { Comments } from "../../ui/layout/Comments";
import {observer} from "mobx-react";
import {MovieCard} from "../../MovieCard";

const options = {
  method: 'GET',
  headers: {
    accept: 'application/json',
    Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxZGQ3NDA1ZjA0Y2JmZjk0ZjM0MGIxZWIzODBkNjc3ZSIsInN1YiI6IjY0ZmYyODEyZTBjYTdmMDEyZWI4MzMyMCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.GtGhLKfel3PZvVyp5QRI47SKVvxqjIVgQB7Any9dALA'
  }
};
const API_KEY = '1dd7405f04cbff94f340b1eb380d677e'; // Replace with your TMDB API key
const TMDB_BASE_URL = 'https://api.themoviedb.org/3';


const Watchlist: FC = () => {
  const [movies, setMovies] = useState<IMovie[]>([]);
  const [popularMovieIds, setPopularMovieIds] = useState<number[]>([]);

  const auth = useInjection<IAuth>(DependencyType.Auth);

  const [showComments, setShowComments] = useState(false);
  const [isClouded, setClouded] = useState(false);

  useEffect(() => {
    const fetchPopularIds = async () => {
      try {
        const response = await fetch('https://api.themoviedb.org/3/movie/popular?language=en-US&page=1', options)
          .then(response => {
            if (!response.ok) {
              throw new Error(`Failed to fetch popular movies`);
            }
            return response.json();
          })
          .catch(err => {
            console.error(err);
            throw new Error(`Error fetching popular movies`);
          });

        findIds(response); // After fetching popular movie ids, call findIds to extract them
      } catch (error) {
        console.error(`Error fetching popular movies:`, error);
      }
    };
    fetchPopularIds();
  }, []); // Empty dependency array to run only once on mount

  useEffect(() => {
    const fetchData = async () => {
      try {
        const moviesData = await Promise.all(
          popularMovieIds.map((movieId) => fetchMovie(movieId)) // Fetch data for each popular movie
        );
        setMovies(moviesData.filter(Boolean) as IMovie[]); // Update movies state with fetched data
      } catch (error) {
        console.error('Error fetching movie data:', error);
      }
    };

    fetchData();
  }, [popularMovieIds]); // Run whenever popularMovieIds changes

  // Функция для рекурсивного поиска id в JSON
  function findIds(response: any) {
    const ids: number[] = [];
    const movieDataArray = response.results; // Access the results property
    for (const movieData of movieDataArray) {
      ids.push(movieData.id);
    }
    setPopularMovieIds(ids);
  }

  const fetchMovie = async (movieId: number) => {
    try {
      const response = await fetch(`${TMDB_BASE_URL}/movie/${movieId}?api_key=${API_KEY}`);
      if (!response.ok) {
        throw new Error(`Failed to fetch movie with ID ${movieId}`);
      }
      const movieData = await response.json();
      return {
        id: movieData.id,
        title: movieData.title,
        overview: movieData.overview,
        posterPath: movieData.poster_path,
        releaseYear: movieData.release_date ? movieData.release_date.split('-')[0] : '',
        imdbRating: movieData.vote_average.toString(),
        ageRating: 'PG-13',
        genre: movieData.genres[0].name,
        country: movieData.production_countries[0].name,
      } as IMovie;
    } catch (error) {
      console.error(`Error fetching movie with ID ${movieId}:`, error);
      return null;
    }
  };

  return (

    <SafeAreaView>
        <Text style={{ marginTop: 20, marginLeft: 15, color: 'white', fontSize: 30, fontWeight: 'bold', marginBottom:25 }}>Popular Now</Text>
        <ScrollView  contentContainerStyle={{ flexDirection: 'column', alignItems: 'center' }}>

            {movies.slice().reverse().map((movie, index) => {

              const movieModel = new Movie(movie.id.toString(), auth.user?.userId.toString() as string);
            return <MovieCard movie={movie} movieModel={movieModel} />}
          )}
        </ScrollView>

      {/* Модальное окно для комментариев */}
    </SafeAreaView>
  );
}

export default Watchlist;
