import { FC } from 'react';
import { Text, View } from 'react-native';
import MenuItem from './MenuItem';
import { menuData } from './menu.data';

interface IBottomMenu {
  nav: any;
  currentRoute?: string;
}

const BottomMenu: FC<IBottomMenu> = (props) => {
  return (
    <View className="pt-5 pb-8 flex-row justify-between items-center w-full bg-[#1E1E1E]">
      {menuData.map((item) => (
        <MenuItem item={item} key={item.path} {...props} />
      ))}
    </View>
  );
};

export default BottomMenu;
