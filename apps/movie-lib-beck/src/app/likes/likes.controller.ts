import { Controller, Param, Post, Get } from '@nestjs/common';
import { LikeService } from './like.service';

@Controller('like')
export class LikesController {
  constructor(private readonly likeService: LikeService) {}

  @Post('add/:userId/:movieId')
  async add(
    @Param('userId') userId: string,
    @Param('movieId') movieId: string
  ) {
    return this.likeService.add(userId, movieId);
  }

  @Post('remove/:userId/:movieId')
  async remove(
    @Param('userId') userId: string,
    @Param('movieId') movieId: string
  ) {
    return this.likeService.remove(userId, movieId);
  }

  @Get('all/:movieId')
  async getLikes(@Param('movieId') movieId: string) {
    return this.likeService.getLikes(movieId);
  }
}
