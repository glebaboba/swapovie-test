import {Controller, Get, Param, Post} from '@nestjs/common';
import {MovieService} from "./movie.service";
import {LikeService} from "../likes/like.service";

@Controller('movie')
export class MovieController {
  constructor(
    private readonly movieService: MovieService,
    private readonly likeService: LikeService
  ) {}

  @Get('userLike/:userId')
  async getMoviesByUserLikes(
    @Param('userId') userId: string,
  ) {
    const likes = await this.likeService.getLikesByUser(userId);

    console.log(likes.map((like) => like.movieId));
    return likes.map((like) => like.movieId)
  }
}
