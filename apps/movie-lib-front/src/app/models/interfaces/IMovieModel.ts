import { ILikeResponse } from './ILikeResponse';
import { ICommentResponse } from './ICommentResponse';

export interface IMovieModel {
  likes: ILikeResponse[];
  comments: ICommentResponse[];
  isLiked: boolean;
  addLike: () => Promise<void>;
  removeLike: () => Promise<void>;
  addComment: (commentText: string) => Promise<void>;
  removeComment: (commentId: string) => Promise<void>;
}
