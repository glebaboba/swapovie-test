import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { Prisma } from '@prisma/client';
import { IAuthResponse } from '@movie-lib/api-interfaces';
import { JwtAuthGuard } from './guards/jwt-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  signIn(
    @Body() userWhereUniqueInput: Prisma.UserWhereUniqueInput
  ): Promise<IAuthResponse> {
    return this.authService.signIn(userWhereUniqueInput);
  }

  @Post('register')
  signUp(
    @Body() userCreateInput: Prisma.UserCreateInput
  ): Promise<IAuthResponse> {
    return this.authService.signUp(userCreateInput);
  }

  @UseGuards(JwtAuthGuard)
  @Get('test')
  test(): string {
    return 'test';
  }
}
