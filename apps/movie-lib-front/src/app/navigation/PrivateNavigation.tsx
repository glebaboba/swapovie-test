import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { FC } from 'react';
import { TypeRootStackParamList } from './navigation.types';
import { useInjection } from 'inversify-react';
import { IAuth } from '../models/interfaces';
import { DependencyType } from '../ioc/DependencyType';
import { routes } from './routes';
import Auth from '../components/screens/auth/Auth';
import { observer } from 'mobx-react';

const Stack = createNativeStackNavigator<TypeRootStackParamList>();

const PrivateNavigation: FC = observer(() => {
  const auth = useInjection<IAuth>(DependencyType.Auth);

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {
          backgroundColor: '#282828',
        },
      }}
    >
      {auth.user ? (
        routes.map((route) => <Stack.Screen key={route.name} {...route} />)
      ) : (
        <Stack.Screen name="Auth" component={Auth} />
      )}
    </Stack.Navigator>
  );
});

export default PrivateNavigation;
