import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { Prisma } from '@prisma/client';
import { BcryptService } from './bcrypt/bcrypt.service';
import { IAuthResponse, IJwtData, IUser } from '@movie-lib/api-interfaces';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private bcryptService: BcryptService
  ) {}

  async signIn(
    userWhereUniqueInput: Prisma.UserWhereUniqueInput
  ): Promise<IAuthResponse> {
    const user = await this.userService.getUser({
      email: userWhereUniqueInput.email,
    });

    return {
      user: user,
      accessToken: await this.getAccessToken(user),
    };
  }

  public async signUp(
    userCreateInput: Prisma.UserCreateInput
  ): Promise<IAuthResponse> {
    const potentialExistingUser = await this.userService.getUser({
      email: userCreateInput.email,
    });

    if (potentialExistingUser)
      throw new BadRequestException('This email already exist');

    const newUser = await this.userService.createUser({
      username: userCreateInput.username,
      email: userCreateInput.email,
      password: await this.bcryptService.hashPassword(userCreateInput.password),
    });

    return {
      user: newUser,
      accessToken: await this.getAccessToken(newUser),
    };
  }

  private async getAccessToken(user: IUser): Promise<string> {
    const data: IJwtData = {
      userId: user.userId,
      email: user.email,
    };

    return await this.jwtService.signAsync(data, {
      expiresIn: '2h',
    });
  }
}
