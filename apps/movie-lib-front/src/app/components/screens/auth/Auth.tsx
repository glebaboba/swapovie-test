import { useState } from 'react';
import {
  Keyboard,
  Pressable,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { SubmitHandler, useForm } from 'react-hook-form';

import { observer } from 'mobx-react';
import { useInjection } from 'inversify-react';
import { DependencyType } from '../../../ioc/DependencyType';
import { IAuth } from '../../../models/interfaces';
import { IAuthForm } from '../../../models/interfaces/IAuthForm';
import Loader from '../../ui/layout/Loader';
import AuthFields from './AuthFields';
import Button from '../../ui/Button';

const Auth = observer(() => {
  const [isReg, setIsReg] = useState(false);
  const authModel = useInjection<IAuth>(DependencyType.Auth);

  const { control, reset, handleSubmit } = useForm<IAuthForm>({
    mode: 'onChange',
  });

  const onSubmit: SubmitHandler<IAuthForm> = (data) => {
    if (isReg) authModel.register(data.email, data.password);
    else authModel.login(data.email, data.password);
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View className="items-center justify-center flex-1">
        <View className="w-3/4">
          <Text className="text-white text-4xl font-bold text-center mb-5">
            {isReg ? 'Sign up' : 'Sign in'}
          </Text>

          {authModel.isLoading ? (
            <Loader />
          ) : (
            <>
              <AuthFields control={control} />
              <Button onPress={handleSubmit(onSubmit)}>GO</Button>

              <Pressable
                onPress={() => setIsReg(!isReg)}
                className="w-16 self-end"
              >
                <Text className="text-opacity-60 text-white text-base mt-3 text-right">
                  {isReg ? 'Login' : 'Register'}
                </Text>
              </Pressable>
            </>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
});

export default Auth;
