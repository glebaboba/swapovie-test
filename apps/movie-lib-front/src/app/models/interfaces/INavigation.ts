export interface INavigation {
  setComponentId: (id: string) => void;
  pushToScreen: (screenName: string) => void;
}
