export const DependencyType = {
  Navigation: Symbol.for('Navigation'),
  Auth: Symbol.for('Auth'),
  User: Symbol.for('User'),
  MovieRepository: Symbol.for('MovieRepository'),
  AuthRepository: Symbol.for('AuthRepository'),
};
