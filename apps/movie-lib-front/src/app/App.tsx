import 'reflect-metadata';
import React from 'react';
import { Provider } from 'inversify-react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { configureContainer } from './ioc/configureContainer';
import Navigation from './navigation/Navigation';

export const App = () => {
  const queryClient = new QueryClient();
  const container = configureContainer();

  return (
    <QueryClientProvider client={queryClient}>
      <Provider container={container}>
        <SafeAreaProvider>
          <Navigation />
        </SafeAreaProvider>
      </Provider>
      <StatusBar style="light" />
    </QueryClientProvider>
  );
};

export default App;
