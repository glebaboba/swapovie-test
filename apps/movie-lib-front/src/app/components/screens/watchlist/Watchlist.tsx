

import {FC, useEffect, useState} from "react";
import {Text, View, Image, FlatList, ScrollView, TouchableOpacity, Modal} from 'react-native'
import {useInjection} from "inversify-react";
import {DependencyType} from "../../../ioc/DependencyType";
import {observer} from "mobx-react-lite";
import {IMovie} from "../../../models/interfaces/IMovie";
import {IUser} from "../../../models/interfaces/IUser";
import {IAuth} from "../../../models/interfaces";
import {SafeAreaView} from "react-native-safe-area-context";
import { AntDesign } from '@expo/vector-icons';
import { Comments } from "../../ui/layout/Comments";
import { Movie } from "../../../models/Movie";

const API_KEY = '1dd7405f04cbff94f340b1eb380d677e'; // Replace with your TMDB API key
const TMDB_BASE_URL = 'https://api.themoviedb.org/3';
const TMDB_DISCOVER_URL = `${TMDB_BASE_URL}/discover/movie`;
const TMDB_LATEST_MOVIE_URL = `${TMDB_BASE_URL}/movie/latest`;


const Watchlist: FC = observer(() => {
  const [movies, setMovie] = useState<IMovie[]>([]);
  const auth = useInjection<IAuth>(DependencyType.Auth);
  const user = useInjection<IUser>(DependencyType.User);

  const [showComments, setShowComments] = useState(false); // добавлено
  const [isClouded, setClouded] = useState(false);


  const toggleLike = () => {
    movieModel.isLiked ? movieModel.removeLike() : movieModel.addLike();
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const moviesIds = await fetchMovieIds();
        const moviesData = await Promise.all(
          moviesIds.map((movieId) => fetchMovie(Number(movieId)))
        );
        setMovie(moviesData.filter(Boolean) as IMovie[]);
      } catch (error) {
        console.error('Error fetching movie data:', error);
      }
    };
    
  
    fetchData();
  }, []);
  

  const fetchMovieIds = async () => {
    return await user.getMoviesByUserLikes(auth.user?.userId.toString() as string)
  };

  const fetchMovieVideo = async (movieId: number) => {
    try {
      const response = await fetch(
        `${TMDB_BASE_URL}/movie/${movieId}/videos?api_key=${API_KEY}`
      );
      const data = await response.json();

      if (data && data.results && data.results.length > 0) {
        return data.results[0].key;
      }

      // Return a default video key when no video data is found
      return 'dQw4w9WgXcQ';
    } catch (error) {
      console.error(`Error fetching video for movie ${movieId}.`, error);
      return 'dQw4w9WgXcQ'; // Use the default video key for error cases
    }
  };

  const fetchMovie = async (movieId: number) => {
    const movieDataResponse = await fetch(
      `${TMDB_BASE_URL}/movie/${movieId}?api_key=${API_KEY}`
    );
    const videoKey = await fetchMovieVideo(Number(movieId));

    if (movieDataResponse.ok && videoKey) {
      const movieData = await movieDataResponse.json();

      if (movieData.id) {
        // Fetch cast information
        const creditsResponse = await fetch(
          `${TMDB_BASE_URL}/movie/${movieId}/credits?api_key=${API_KEY}`
        );
        const creditsData = await creditsResponse.json();

        return {
          id: movieData.id,
          title: movieData.title,
          overview: movieData.overview,
          videoKey,
          posterPath: movieData.poster_path,
          releaseYear: movieData.release_date
            ? movieData.release_date.split('-')[0]
            : '',
          imdbRating: movieData.vote_average.toString(),
          ageRating: 'PG-13',
          genre: movieData.genres[0].name,
          country: movieData.production_countries[0].name,
          cast: creditsData.cast.map((castMember: any) => ({
            id: castMember.id,
            name: castMember.original_name,
            profile_path: castMember.profile_path,
          })),
        } as IMovie
      }
    }
  }

  
  return (
    <SafeAreaView>
      <Text style={{marginLeft: 15, color: 'white', fontSize: 30, fontWeight: 'bold', marginBottom:25 }}>Your Likes</Text>
      <ScrollView contentContainerStyle={{ flexDirection: 'column', alignItems: 'center' }}>
        {movies.slice().reverse().map((movie, index) => {
            const movieModel = new Movie(
                movie.id.toString(),
                auth.user?.userId.toString() || ''
              );

          return (
            <View key={index} style={{ padding: 5, backgroundColor: "#1c1c1c", marginBottom: 10 , borderRadius: 15, width: '90%', }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ flex: 1, transform: [{ scale: 0.9 }], padding: 10 }}>
                  <View style={{ aspectRatio: 2 / 3, overflow: 'hidden' }}>
                    <Image
                      source={{ uri: `https://image.tmdb.org/t/p/w500/${movie.posterPath}` }}
                      style={{ width: '100%', height: '100%',  borderRadius: 20}}
                    />
                  </View>
                </View>
                <View style={{ flex: 3, paddingLeft: 10 }}>
                  {/* Text and information */}
                  <Text style={{ color: 'white' }}>
                    {movie.releaseYear + `  ${movie.imdbRating} (iMDB)`}
                  </Text>
                  <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>
                    {movie.title}
                  </Text>
                  <Text style={{ color: 'white' }}>{movie.genre}</Text>
                  <Text style={{ color: 'white' }}>{movie.ageRating}</Text>
                  <Text style={{ color: 'white' }}>{movie.country}</Text>
                </View>
              </View>
               {/*Block 2: Buttons */}

               <View
                  style={{
                    height: 'auto',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: '2%',
                    paddingTop: '5%',
                  }}
                >
                  <TouchableOpacity
                    onPress={toggleLike}
                    style={{
                      alignItems: 'center',
                      paddingRight: '3%',
                      paddingLeft: '2%',
                      flexDirection: 'row',
                      backgroundColor: movieModel.isLiked ? 'white' : '#1E1E1E',
                      minHeight: 35,
                      borderRadius: 7,
                      marginRight: '3%',
                    }}
                  >
                    <AntDesign
                      name="like1"
                      size={20}
                      color={movieModel.isLiked ? '#1E1E1E' : 'white'}
                      style={{ paddingRight: 5 }}
                    />
                    <Text style={{ color: movieModel.isLiked ? '#1E1E1E' : 'white' }}>
                      {movieModel.likes.length}
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => setShowComments(true)} // изменено
                    style={{
                      alignItems: 'center',
                      paddingRight: '3%',
                      paddingLeft: '2%',
                      flexDirection: 'row',
                      backgroundColor: isClouded ? 'white' : '#1E1E1E',
                      minHeight: 35,
                      borderRadius: 7,
                      marginRight: '3%',
                    }}
                  >
                    <AntDesign
                      name="message1"
                      size={20}
                      color={isClouded ? '#1E1E1E' : 'white'}
                      style={{ paddingRight: 5 }}
                    />
                    <Text style={{ color: isClouded ? '#1E1E1E' : 'white' }}>
                      {movieModel.comments.length}
                    </Text>
                  </TouchableOpacity>
                </View>
            </View>
           
          )
        })}
      </ScrollView>

      
    </SafeAreaView>
  )
  
  
})

export default Watchlist


/*
<View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
                <TouchableOpacity
                  onPress={toggleLike}
                  style={{
                    alignItems: 'center',
                    paddingRight: '3%',
                    paddingLeft: '2%',
                    flexDirection: 'row',
                    backgroundColor: movieModel.isLiked ? 'white' : '#1E1E1E',
                    minHeight: 35,
                    borderRadius: 7,
                    marginRight: '3%',
                  }}
                >
                  <AntDesign
                    name="like1"
                    size={20}
                    color={movieModel.isLiked ? '#1E1E1E' : 'white'}
                    style={{ paddingRight: 5 }}
                  />
                  <Text style={{ color: movieModel.isLiked? '#1E1E1E' : 'white' }}>
                  {movieModel.likes.length}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
              onPress={toggleCloud}
              style={{
                alignItems: 'center',
                paddingRight: '3%',
                paddingLeft: '2%',
                flexDirection: 'row',
                backgroundColor: isClouded ? 'white' : '#1E1E1E',
                minHeight: 35,
                borderRadius: 7,
                marginRight: '3%',
              }}
            >
              <AntDesign
                name="message1"
                size={20}
                color={isClouded ? '#1E1E1E' : 'white'}
                style={{ paddingRight: 5 }}
              />
              <Text style={{ color: isClouded ? '#1E1E1E' : 'white' }}>
                {movieModel.comments.length}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ padding: 10, paddingTop: 20, height: 200 }}>
            <TextInput
              style={{
                borderStyle: 'solid',
                borderColor: '#fff',
                borderWidth: 2,
                height: 50,
              }}
              onChangeText={(value) => setComment(value)}
            />
            <Button
              onPress={() => movieModel.addComment(comment)}
              title="Send"
            />

            <FlatList
              data={movieModel.comments}
              renderItem={({ item }) => (
                <View>
                  <Text>{item.text}</Text>
                </View>
              )}
              keyExtractor={(item) => item.commentId.toString()}
            />
          </View>
        </View>
*/