import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class CommentsService {
  constructor(private prisma: PrismaService) {}

  async add(userId: string, movieId: string, commentText: string) {
    const user = await this.prisma.user.findUnique({
      where: {
        userId: Number(userId),
      },
    });
    if (!user) throw new NotFoundException('User does not exist');

    await this.prisma.comment.create({
      data: {
        userId: Number(userId),
        movieId: Number(movieId),
        text: commentText,
      },
    });

    return this.prisma.comment.findMany({
      where: {
        movieId: Number(movieId),
      },
    });
  }

  async remove(commentId: string) {
    const deletedComment = await this.prisma.comment.delete({
      where: {
        commentId: Number(commentId),
      },
    });

    return this.prisma.comment.findMany({
      where: {
        movieId: Number(deletedComment.movieId),
      },
    });
  }

  async getComments(movieId: string) {
    const likes = await this.prisma.comment.findMany({
      where: {
        movieId: Number(movieId),
      },
    });

    return likes || [];
  }
}
