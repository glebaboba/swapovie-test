import { FC, PropsWithChildren } from "react";
import {Platform, Text, View} from 'react-native'
import { SafeAreaView, useSafeAreaFrame, useSafeAreaInsets } from "react-native-safe-area-context";

const Layout: FC<PropsWithChildren<unknown>> = ({children, title}) => {
    const {top} = useSafeAreaInsets ()
    return (
        <SafeAreaView className='flex-1'>
            <View className='flex-1 px-6' style={{
                paddingTop: Platform.OS === 'ios' ? top /5 : top * 1.6
            }}>
                {title && (<Text className = 'text-2xl text-center font-semibold text-white'>{title}</Text>)}
                {children}
            </View>
        </SafeAreaView>
    )
}

export default Layout