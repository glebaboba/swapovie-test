import { useInjection } from 'inversify-react';
import { DependencyType } from '../../../ioc/DependencyType';
import { IAuth } from '../../../models/interfaces';
import Layout from '../../ui/layout/Layout';
import Button from '../../ui/Button';
import { IUser } from '../../../models/interfaces/IUser';
import { useEffect } from 'react';
import { View, Text } from 'react-native';

const Profile = () => {
  const authModel = useInjection<IAuth>(DependencyType.Auth);
  const auth = useInjection<IAuth>(DependencyType.Auth);
  const user = useInjection<IUser>(DependencyType.User);


  const fetchMovieIds = async () => {
    return await user.getMoviesByUserLikes(auth.user?.userId.toString() as string)
  };

  return (
    <Layout>
      <View>
        <Button onPress={authModel.logout}>Logout</Button>
        <Text>{JSON.stringify(fetchMovieIds)}</Text>
      </View>
      
    </Layout>
  );
};

export default Profile;
