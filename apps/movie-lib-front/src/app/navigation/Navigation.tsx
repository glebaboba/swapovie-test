import {
  NavigationContainer,
  useNavigationContainerRef,
} from '@react-navigation/native';
import { FC, useEffect, useState } from 'react';
import PrivateNavigation from './PrivateNavigation';
import { useInjection } from 'inversify-react';
import { IAuth } from '../models/interfaces';
import { DependencyType } from '../ioc/DependencyType';
import BottomMenu from '../components/ui/layout/bottom-menu/BottomMenu';
import { observer } from 'mobx-react';

const Navigation: FC = observer(() => {
  const auth = useInjection<IAuth>(DependencyType.Auth);

  const [currentRoute, setCurrentRoute] = useState<string | undefined>(
    undefined
  );

  const navRef = useNavigationContainerRef();

  useEffect(() => {
    setCurrentRoute(navRef.getCurrentRoute()?.name);

    const listener = navRef.addListener('state', () =>
      setCurrentRoute(navRef.getCurrentRoute()?.name)
    );

    return () => {
      navRef.removeListener('state', listener);
    };
  }, []);
  return (
    <>
      <NavigationContainer ref={navRef}>
        <PrivateNavigation />
      </NavigationContainer>
      {auth.user && currentRoute && (
        <BottomMenu nav={navRef.navigate} currentRoute={currentRoute} />
      )}
    </>
  );
});

export default Navigation;
