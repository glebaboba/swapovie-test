import { FC } from 'react';
import { ActivityIndicator, Text, View } from 'react-native';

const Loader: FC = () => {
  return <ActivityIndicator color={'#FFFFFF'} size="large" />;
};

export default Loader;
