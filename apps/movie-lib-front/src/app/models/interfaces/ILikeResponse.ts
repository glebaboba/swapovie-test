export interface ILikeResponse {
    likeId: number
    userId: number
    movieId: number
}
