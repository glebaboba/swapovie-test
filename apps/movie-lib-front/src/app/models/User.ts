import {injectable} from "inversify";
import {IUser} from "./interfaces/IUser";
import {makeAutoObservable} from "mobx";
import instance from "../utils/interceptor";


@injectable()
export class User implements IUser {
  public constructor() {
    makeAutoObservable(this);
  }

  public async getMoviesByUserLikes(userId: string): Promise<string[]> {
    const response = await instance.get<string[]>(`/movie/userLike/${userId}`);

    return  response.data
  }
}
