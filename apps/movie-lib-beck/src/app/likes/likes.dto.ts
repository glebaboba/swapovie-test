import {IsEmail, IsNumber, IsString, MinLength} from "class-validator";

export class Like {
    @IsEmail()
    email: string

    @MinLength(6,{
        message:'Too short value'
    })

    @IsString()
    password: string
}

