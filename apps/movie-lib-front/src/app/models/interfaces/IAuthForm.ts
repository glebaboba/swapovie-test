import { IUser } from '@movie-lib/api-interfaces';

export interface IAuthForm extends Pick<IUser, 'email' | 'password'> {}
