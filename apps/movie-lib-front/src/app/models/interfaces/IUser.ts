export interface IUser {
  getMoviesByUserLikes: (userId: string) => Promise<string[]>
}
