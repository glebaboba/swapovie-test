import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {ICommentResponse} from "../../../models/interfaces/ICommentResponse";

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

interface Comment {
    userId: string;
    commentId: string;
    text: string;
    createdAt: Date;
}

interface CommentsProps {
  comments: ICommentResponse[];
  onClose: () => void;
  addComment: (comment: string) => void;
}

const getDaysDifference = (date: Date): string => {
    const now = new Date();
    const differenceInTime = now.getTime() - date.getTime();
    const differenceInDays = Math.floor(differenceInTime / (1000 * 3600 * 24));

    if (differenceInDays === 0) {
      return 'Today';
    } else {
      return `${differenceInDays} days ago`;
    }
  };

export const Comments: React.FC<CommentsProps> = ({ comments, onClose, addComment }) => {
  const [comment, setComment] = useState('');

  const handleAddComment = () => {
    if (comment.trim().length > 0) {
      addComment(comment);
      setComment(''); // Очистить поле после добавления комментария
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onClose} style={styles.closeButton}>
        <Text style={styles.closeButtonText}>Close</Text>
      </TouchableOpacity>
      <FlatList
        data={comments}
        renderItem={({ item }) => (
          <View style={styles.commentContainer}>
            <Text style = {{color: 'white', fontSize: 15, opacity: 0.6, marginBottom: 5}}>id{item.userId}</Text>
            <Text style = {{fontWeight: 'bold', color: 'white', fontSize: 18, marginBottom: 7}}>{item.text}</Text>
            {/*// @ts-ignore*/}
            <Text style = {{color: 'white', fontSize: 15,opacity: 0.6}}>{getDaysDifference(new Date(item.createdAt))}</Text>
          </View>
        )}
        keyExtractor={(item) => item.commentId.toString()}
      />
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          value={comment}
          onChangeText={setComment}
          placeholder="Write a comment..."
          placeholderTextColor="gray"

        />
        <Button title="Send" onPress={handleAddComment} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: screenHeight * 0.75,
    marginTop: 'auto',
    backgroundColor: '#1c1c1c',
    padding: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,

  },
  closeButton: {
    alignSelf: 'flex-end',
    padding: 10,
  },
  closeButtonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '600'
  },
  commentContainer: {
    marginBottom: 30,

  },
  inputContainer: {
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    color: 'white',
  },
  input: {
    fontSize: 18,
    color: 'white',
    flex: 1,
    backgroundColor: '#121212',
    height: 45,
    borderRadius: 10,
    paddingHorizontal: 10,
    marginRight: 10,

  },
});
