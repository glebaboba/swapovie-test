import { ICastMember } from './ICastMember';

export interface IMovie {
  id: number;
  title: string;
  overview: string;
  videoKey: string;
  posterPath: string;
  releaseYear: string;
  imdbRating: string;
  ageRating: string;
  genre: string;
  country: string;
  cast: ICastMember[];
  description: string;
}
