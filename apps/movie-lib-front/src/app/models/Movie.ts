import {
  action,
  computed,
  makeAutoObservable,
  makeObservable,
  observable,
  runInAction,
} from 'mobx';
import { IMovieModel } from './interfaces/IMovieModel';
import { ILikeResponse } from './interfaces/ILikeResponse';
import { ICommentResponse } from './interfaces/ICommentResponse';
import instance from '../utils/interceptor';

export class Movie implements IMovieModel {
  @observable
  private readonly movieId: string = '';

  @observable
  private readonly userId: string = '';

  @observable
  private isLoading: boolean = false;

  @observable
  private likesData: ILikeResponse[] = [];

  @observable
  private commentsData: ICommentResponse[] = [];

  constructor(movieId: string, userId: string) {
    makeAutoObservable(this);

    this.movieId = movieId;
    this.userId = userId;
    this.isLoading = true;
    this.getLikes();
    this.getComments();
    this.isLoading = false;
  }

  @computed
  public get likes(): ILikeResponse[] {
    return this.likesData;
  }

  @computed
  public get comments(): ICommentResponse[] {
    return this.commentsData;
  }

  @computed
  public get isLiked(): boolean {
    return this.likes.some((like) => like.userId.toString() === this.userId);
  }

  @action
  public async addLike() {
    this.isLoading = true;

    const response = await instance.post<ILikeResponse[]>(
      `/like/add/${this.userId}/${this.movieId}`
    );

    runInAction(() => {
      this.likesData = response.data;
      this.isLoading = false;
    });

    console.log(this.likesData);
  }

  @action
  public async removeLike() {
    this.isLoading = true;

    const response = await instance.post<ILikeResponse[]>(
      `/like/remove/${this.userId}/${this.movieId}`
    );

    runInAction(() => {
      this.likesData = response.data;
      this.isLoading = false;
    });
  }

  @action
  private async getLikes() {
    const response = await instance.get<ILikeResponse[]>(
      `/like/all/${this.movieId}`
    );

    runInAction(() => {
      this.likesData = response.data;
    });
  }

  @action
  public async addComment(commentText: string) {
    this.isLoading = true;

    const response = await instance.post<ICommentResponse[]>(
      `/comments/add/${this.userId}/${this.movieId}`,
      { commentText }
    );

    runInAction(() => {
      this.commentsData = response.data;
      this.isLoading = false;
    });
  }

  @action
  public async removeComment(commentId: string) {
    this.isLoading = true;

    const response = await instance.post<ICommentResponse[]>(
      `/comments/remove/${commentId}`
    );

    runInAction(() => {
      this.commentsData = response.data;
      this.isLoading = false;
    });
  }

  @action
  private async getComments() {
    const response = await instance.get<ICommentResponse[]>(
      `/comments/all/${this.movieId}`
    );

    runInAction(() => {
      this.commentsData = response.data;
    });
  }
}
