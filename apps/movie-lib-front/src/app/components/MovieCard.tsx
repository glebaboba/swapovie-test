import {Image, Modal, Text, TouchableOpacity, View} from "react-native";
import {AntDesign} from "@expo/vector-icons";
import React, {useEffect, useState} from "react";
import {IMovie} from "../models/interfaces/IMovie";
import {Movie} from "../models/Movie";
import {IMovieModel} from "../models/interfaces/IMovieModel";
import {observer} from "mobx-react";
import {Comments} from "./ui/layout/Comments";

interface IProps {
  movie: IMovie
  movieModel: IMovieModel
}

export const MovieCard = observer((props: IProps) => {
  const { movie, movieModel } = props

  const [showComments, setShowComments] = useState(false);

  return (
    <View style={{padding: 5, backgroundColor: "#1c1c1c", marginBottom: 10, borderRadius: 15, width: '90%',}}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <View style={{flex: 1, transform: [{scale: 0.9}], padding: 10}}>
          <View style={{aspectRatio: 2 / 3, overflow: 'hidden'}}>
            <Image
              source={{uri: `https://image.tmdb.org/t/p/w500/${movie.posterPath}`}}
              style={{width: '100%', height: '100%', borderRadius: 20}}
            />
          </View>
        </View>
        <View style={{flex: 3, paddingLeft: 10}}>
          {/* Text and information */}
          <Text style={{color: 'white'}}>
            {movie.releaseYear + `  ${movie.imdbRating} (iMDB)`}
          </Text>
          <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
            {movie.title}
          </Text>
          <Text style={{color: 'white'}}>{movie.genre}</Text>
          <Text style={{color: 'white'}}>{movie.ageRating}</Text>
          <Text style={{color: 'white'}}>{movie.country}</Text>
        </View>
      </View>
      {/*Block 2: Buttons */}

      <View
        style={{
          height: 'auto',
          flexDirection: 'row',
          alignItems: 'center',
          paddingLeft: '2%',
          paddingTop: '5%',
        }}
      >
        <TouchableOpacity
          onPress={() => movieModel.isLiked ? movieModel.removeLike() : movieModel.addLike()}
          style={{
            alignItems: 'center',
            paddingRight: '3%',
            paddingLeft: '2%',
            flexDirection: 'row',
            backgroundColor: movieModel.isLiked ? 'white' : '#1E1E1E',
            minHeight: 35,
            borderRadius: 7,
            marginRight: '3%',
          }}
        >
          <AntDesign
            name="like1"
            size={20}
            color={movieModel.isLiked ? '#1E1E1E' : 'white'}
            style={{paddingRight: 5}}
          />
          <Text style={{color: movieModel.isLiked ? '#1E1E1E' : 'white'}}>
            {movieModel.likes.length}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => setShowComments(true)} // изменено
          style={{
            alignItems: 'center',
            paddingRight: '3%',
            paddingLeft: '2%',
            flexDirection: 'row',
            backgroundColor: 'white',
            minHeight: 35,
            borderRadius: 7,
            marginRight: '3%',
          }}
        >
          <AntDesign
            name="message1"
            size={20}
            color={'white'}
            style={{paddingRight: 5}}
          />
          <Text style={{color: 'white'}}>
            {movieModel.comments.length}
          </Text>
        </TouchableOpacity>
      </View>

      <Modal
        visible={showComments}
        transparent={true}
        animationType="slide"
        onRequestClose={() => setShowComments(false)}
      >
        <Comments
          comments={movieModel.comments}
          onClose={() => setShowComments(false)}
          addComment={(comment) => {
            movieModel.addComment(comment);
          }}
        />
      </Modal>
    </View>
  );

})
